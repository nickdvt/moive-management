const express = require("express");
const app  = express();
const router = require('./routers');

const port = 4200;
app.use(express.json());

app.use(router);


app.get('/', (req, res) => {
    res.status(200).send("Hello!")
});


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});