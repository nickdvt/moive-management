const express = require('express');
const movieRouter = express.Router();
const { getListMovie, getDetailMovieByID, createNewMovie, updateMovieByID, deleteMovieByID } = require('./../controllers/movie.controller');


// Lấy danh sách phim
movieRouter.get('/', getListMovie)

// Lấy chi tiết phim
movieRouter.get('/:id',getDetailMovieByID);

// Thêm phim
movieRouter.post('/',createNewMovie);

// Cập nhật phim
movieRouter.put('/:id',updateMovieByID);

// Xoá phim
movieRouter.delete('/:id',deleteMovieByID);

module.exports = movieRouter;