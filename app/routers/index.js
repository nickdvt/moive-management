const express = require('express');
const movieRouter = require('./movie.router');

const router = express.Router();

// http://localhost:4200/api/movies
router.use('/api/movies',movieRouter);

module.exports = router;
