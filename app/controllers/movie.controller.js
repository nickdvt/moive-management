const { getList, getDetail, createMovie, updateMovie, deleteMovie } = require('./../services/movie.service');

require('./../services/movie.service');

const getListMovie = (req, res) => {
    const listMovie = getList();
    if(listMovie){
        res.status(200).send(listMovie)
    }
    res.status(404).send('Not Found!')
};

const getDetailMovieByID = (req, res) => {
    const {id} = req.params;
    const movieDetail = getDetail(id);
    if(movieDetail){
        res.status(200).send(movieDetail )
    }
    res.status(404).send("Not Found!")
};

const createNewMovie = (req, res) => {
    const movie = req.body;
    const newMovie = createMovie(movie);
    
    if(newMovie){
        res.status(201).send(newMovie);
    };
    res.status(404).send("Not Found!");
};

const updateMovieByID = (req, res) => {
    const {id} = req.params;
    const movie= req.body;
    const movieUpdate = updateMovie(id, movie);
    if(movieUpdate){
        res.status(200).send(movieUpdate)
    }
    res.status(404).send("Not Found!");
};

const deleteMovieByID = (req, res) => {
    const {id} = req.params;
    const movieDelete = deleteMovie(id)
    if(movieDelete) {
        res.status(200).send(movieDelete);
    }
    res.status(404).send("Not Found!");
};

module.exports = {
    getListMovie,
    getDetailMovieByID,
    createNewMovie,
    updateMovieByID,
    deleteMovieByID
}