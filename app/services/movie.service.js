let listMovie = [
    {
        id: "1",
        name: "Rasing Hope",
        totalMovieTime: "125 phút",
        poster: "http://movie0706.cybersoft.edu.vn/hinhanh/rasing-hope_gp05.jpg",
        trailer: "https://youtube.com/embed/7V7SBjaQQ4g"
    },
    {
        id: "2",
        name: "Bố Già",
        totalMovieTime: "120 phút",
        poster: "https://www.youtube.com/embed/jluSu8Rw6YE",
        trailer: "http://movie0706.cybersoft.edu.vn/hinhanh/bo-gia_gp01.jpg"
    }
];

const getList = () => {
    if(listMovie){
        return listMovie
    };
    return false;
};

const getDetail = (id) => {
    const movieDetail = listMovie.find(movie => movie.id == id);
    if(movieDetail){
        return movieDetail;
    }
    return false;
}

const createMovie = (Movie) => {
    const newMovie = {
        id: Math.floor(Math.random()*1000).toString(),
        ...Movie
    };
    listMovie = [...listMovie, newMovie];
    if(newMovie){
        return newMovie;
    };
    return false;
}

const updateMovie = (id, movie) => {
    const index = listMovie.findIndex(movie => movie.id == id);
    if(index !== -1){
        let movieUpdate = {...listMovie[index], ...movie};
        listMovie[index] = movieUpdate;
        return movieUpdate;
    };
    return false;
}

const deleteMovie = (id) => {
    const index = listMovie.findIndex((movie) => movie.id == id);
    if(index !== -1) {
        const movieDelete = listMovie[index];
        listMovie.splice(index, 1);
        return movieDelete;
    };
    return false;
}

module.exports = {
    getList,
    getDetail,
    createMovie,
    updateMovie,
    deleteMovie
}